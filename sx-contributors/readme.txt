=== Contributors ===
Plugin Name:  Contributors
Version:      0.0.2
Plugin URI:   http://www.seomix.fr
Description:  A WordPress plugin to display an admin page showing all contributors.
Usage: No configuration necessary. Upload, activate and done.
Availables languages : en_EN, fr_FR
Tags: login, wp-login, admin, contributors, members
Author: Daniel Roch
Author URI: http://www.seomix.fr
Contributors: Confridin
Requires at least: 4.4
Tested up to: 4.7.1
Stable tag: trunk
License: GPL v3

A WordPress plugin to display an admin page showing all contributors.

== Description ==

A WordPress plugin to display an admin page showing all contributors. After user login, he will be redirected to this contributors page.

This plugin is a use case to show what can be done to create a community. It can also be used as a member directory basis.

Feel free to contribute on BitBucket : https://bitbucket.org/seomix/contributors

Daniel Roch - http://www.seomix.fr

== Installation ==

No configuration is necessary. Upload, activate and done.

== Screenshots ==

1. Main admin page for Contributors Plugin
2. Second page showing active users

== Changelog ==

= 0.0.2 =
* translation ready
* better french translation ("plugin" => "extension")
* adding post number
* reduce description length if user description is too long
* order change : users are now listed with Post Count DESC

= 0.0.1 =
* first release

== Frequently Asked Questions ==

= Do I need to do anything else for this to work? =

No : just install it ;)