# README #

Contributors is a WordPress plugin : [Contributors Plugin](https://wordpress.org/plugins/contributors/)

Created by Daniel Roch - [SeoMix](http://www.seomix.fr)

### What is this repository for? ###

Contributors is a  WordPress plugin that display an admin page showing all contributors. After user login, he will be redirected to the contributors page.

This plugin is a use case to show what can be done to create a community.